## Installation

1. Ruby on Rails

    https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-16-04

    Additional: When you execute above link's command please set Ruby version is 2.5.1 and Rails 6.0.2.1

2. PostgreSQL

    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04

    - *UBUNTU*: create an own PostgreSQL user with following command.

            #!bash

            sudo -u postgres createuser --interactive.*
            # and set password to user.
            psql:\password

    - *MAC*: Nothing to do.

3. Prerequisites:
   * Install NodeJs
   * npm install yarn
   * yarn install

4. Run the following command to setup system locally
   * git clone https://mohdanas@bitbucket.org/mohdanas/camera-store.git
   * bundle install
   * rails db:create
   * rails db:migrate

5. Live Demo

  https://camerastore-crownstack.herokuapp.com

6. API Documentation

   https://documenter.getpostman.com/view/8716066/SWT7BevL?version=latest

7. System Setup

  * Visit https://camerastore-crownstack.herokuapp.com and use sign up link to register user.
  * It will redirect you admin dashboard where you can register categories and products.

8. Repository link

   https://bitbucket.org/mohdanas/camera-store/src/master/
