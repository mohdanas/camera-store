# frozen_string_literal: true

class Category < ApplicationRecord
  # type is restricted word, you can't use it as a column name in ActiveRecord models (unless you're doing STI).
  self.inheritance_column = :camera_type

  has_many :products

  validates_presence_of :name, :model, :type

  rails_admin do
    field :name do
      help nil
    end
    field :model
    create do
      field :type, :enum do
        enum do
          ['Mirrorless', 'Full Frame', 'Point and Shoot']
        end
      end
    end
  end
end
