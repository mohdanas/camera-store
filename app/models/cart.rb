# frozen_string_literal: true

class Cart < ApplicationRecord
  has_many :cart_items, dependent: :destroy

  # Add an item to a cart.
  def add_product(product_id, item_quantity)
    item_quantity = item_quantity.to_i
    if cart_has_item(product_id)
      # Add an item that already exists in the cart, it will update the cureent item quantity
      current_item_quantity = item_quantity(product_id)
      update_cart_item(product_id, current_item_quantity + item_quantity)
    else
      cart_items.create!(product_id: product_id, item_quantity: item_quantity)
    end
  end

  # Attempts to update the quantity of an item in the cart.
  def update_cart_item(product_id, item_quantity)
    item_quantity = item_quantity.to_i

    cart_item = cart_items.find_by!(product_id: product_id.to_i)
    cart_item.update!(item_quantity: item_quantity)
  end

  private

  # Returns a boolean indicating whether an item already exists in the cart or not.
  def cart_has_item(product_id)
    !cart_items.find_by(product_id: product_id.to_i).nil?
  end

  def item_quantity(product_id)
    cart_item = cart_items.find_by!(product_id: product_id.to_i)
    cart_item.item_quantity
  end
end
