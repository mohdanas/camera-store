# frozen_string_literal: true

module Api
  module V1
    class ProductsController < Api::V1::BaseController
      # GET   /api/v1/products
      def index
        @products = Product.all
        respond_with @products
      end
    end
  end
end
