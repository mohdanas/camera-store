# frozen_string_literal: true

module Api
  module V1
    class CartItemsController < Api::V1::BaseController
      # POST   /api/v1/carts/:cart_id/cart_items
      # body ex:
      # {
      #   "product_id": 2,
      #   "cart_id": 1,
      #   "item_quantity": 10
      # }
      def create
        # ensures that all the required parameters are there
        validate_product_id
        validate_cart_id
        validate_item_quantity

        cart = Cart.find(params[:cart_id])
        cart.add_product(params[:product_id], params[:item_quantity])

        render json: { status: 'SUCCESS', message: 'Item added to cart.' },
               status: :created
      end

      private

      # Ensures the item quantity is passed with the request
      def validate_item_quantity
        params.require(:item_quantity)
      end

      # Ensures that the item exists in the cart
      def validate_cart_item
        cart_item = CartItem.find_by(product_id: params[:product_id])
        raise ArgumentError, 'The item does not exist in the cart' if cart_item.nil?
      end
    end
  end
end
