# frozen_string_literal: true

module Api
  module V1
    class BaseController < ActionController::API
      before_action :doorkeeper_authorize! # equivalent of authenticate_user!
      before_action :set_cart
      respond_to :json

      # will send error if access_token is not valid
      def doorkeeper_unauthorized_render_options(error: nil)
        { json: { error: 'Not authorized' } }
      end

      protected

      # Ensures that the product_id parameter exists in the request and Product table
      def validate_product_id
        params.require(:product_id)
        product_id = params[:product_id].to_i # prevents SQL injection
        raise ArgumentError, 'Product not found.' unless Product.exists?(product_id)
      end

      # Ensures that the cart_id parameter exists in the request and Cart table
      def validate_cart_id
        params.require(:cart_id)
        cart_id = params[:cart_id].to_i # prevents SQL injection
        raise ArgumentError, 'Cart not found.' unless Cart.exists?(cart_id)
      end

      private

      def current_user
        @current_user ||= User.find(doorkeeper_token[:resource_owner_id])
      end

      def set_cart
        @cart = Cart.find(session[:cart_id])
      rescue ActiveRecord::RecordNotFound
        @cart = Cart.create
        session[:cart_id] = @cart.id
      end
    end
  end
end
