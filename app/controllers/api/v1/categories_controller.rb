# frozen_string_literal: true

module Api
  module V1
    class CategoriesController < Api::V1::BaseController
      rescue_from ActiveRecord::RecordNotFound, with: :category_not_found

      # GET   /api/v1/categories
      def index
        @categories = Category.all
        respond_with @categories
      end

      # GET /api/v1/categories/:id/products
      def products
        fetch_category
        @products = @category.products
        respond_with @products
      end

      private

      def fetch_category
        @category = Category.find(params[:id])
      end

      def category_not_found
        msg = 'Category not found!'
        render json: { errors: [msg] }, status: 404
      end
    end
  end
end
