# frozen_string_literal: true

module Api
  module V1
    class CartsController < Api::V1::BaseController
      # GET    /api/v1/carts/:cart_id
      def show
        validate_cart_id

        cart = Cart.find(params[:cart_id])

        render json: { status: 'SUCCESS', message: 'Loaded cart content',
                       cart_content: cart.cart_items }, status: :ok
      end
    end
  end
end
