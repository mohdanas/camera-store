FROM ruby:2.5.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /camera-store
WORKDIR /camera-store

COPY Gemfile /camera-store/Gemfile
COPY Gemfile.lock /camera-store/Gemfile.lock

RUN bundle install

COPY . /camera-store
