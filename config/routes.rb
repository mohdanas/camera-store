# frozen_string_literal: true

Rails.application.routes.draw do
  resources :room_messages
  resources :rooms
  use_doorkeeper do
    skip_controllers :authorizations, :applications,
      :authorized_applications
  end

  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  root controller: :rooms, action: :index

  resources :room_messages
  resources :rooms  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      resources :categories, only: [:index] do
        member do
          get 'products'
        end
      end
      resources :carts, only: [:show], param: :cart_id
      resources :carts, only: [:create] do
        resources :cart_items, only: %i[create], param: :product_id
      end
      resources :products, only: [:index]
    end
  end
end
